package com.runrace.server.dao;

import com.runrace.server.model.Account;
import com.runrace.server.model.PasswordRecover;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PasswordRecoverRepository extends JpaRepository<PasswordRecover, Long> {

    Optional<PasswordRecover> findByAccount(Account account);

}
