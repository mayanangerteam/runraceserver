package com.runrace.server.dao;

import com.runrace.server.model.Horse;
import com.runrace.server.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface HorseRepository extends JpaRepository<Horse, Long> {

    Collection<Horse> findByAccount(Account account);

    @Query(value = "SELECT * FROM `horse` ORDER BY RAND() LIMIT :limit", nativeQuery = true)
    Collection<Horse> randomHorses(@Param("limit") int limit);

}
