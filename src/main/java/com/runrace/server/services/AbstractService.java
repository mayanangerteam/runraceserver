package com.runrace.server.services;

import com.runrace.server.application.security.accountimpl.AccountDetails;
import com.runrace.server.application.security.accountimpl.AccountDetailsService;
import com.runrace.server.dao.HorseRepository;
import com.runrace.server.dao.PasswordRecoverRepository;
import com.runrace.server.dao.RaceRepository;
import com.runrace.server.dao.AccountRepository;
import com.runrace.server.model.Account;
import com.runrace.server.model.PasswordRecover;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.Request;
import com.runrace.server.model.connection.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.Executor;

public abstract class AbstractService<Req extends Request, Res extends Response> implements RunraceService<Req, Res> {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private HorseRepository horseRepository;
    @Autowired
    private RaceRepository raceRepository;
    @Autowired
    private PasswordRecoverRepository passwordRecoverRepository;

    @Autowired
    private Executor executor;
    @Autowired
    private JavaMailSender mailSender;

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    public HorseRepository getHorseRepository() {
        return horseRepository;
    }

    public RaceRepository getRaceRepository() {
        return raceRepository;
    }

    public PasswordRecoverRepository getPasswordRecoverRepository() {
        return passwordRecoverRepository;
    }


    public Executor getExecutor() {
        return executor;
    }

    public JavaMailSender getMailSender() {
        return mailSender;
    }

    @Override
    public Res execute(Req request, HttpServletRequest httpServletRequest) {

        Account account = null;
        if (httpServletRequest != null &&
                httpServletRequest.getUserPrincipal() != null &&
                httpServletRequest.getUserPrincipal() instanceof OAuth2Authentication) {
            account = ((AccountDetails) ((OAuth2Authentication) httpServletRequest.getUserPrincipal()).getPrincipal()).getAccount();
        }

        if (request == null) {
            return (Res) new Response(StatusCode.ERROR, ErrorCode.MISSING_DATA);
        }

        return doExecute(request, account, httpServletRequest);
    }

    protected abstract Res doExecute(Req request, Account account, HttpServletRequest httpServletRequest);


    /**
     *
     * @param exception from transaction
     * @return true if exception triggered from an sql duplicate exception
     */
    public static boolean isDuplicateException(Throwable exception) {
        Throwable t = exception;
        while (t.getCause() != null) {
            t = t.getCause();
        }
        return t.getMessage().toLowerCase().contains("duplicate"); // TODO maybe find database codes instead of a "duplicate" string
    }

}
