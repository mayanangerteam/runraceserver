package com.runrace.server.services.api.intr;

import com.runrace.server.model.connection.request.api.ApproveRecoverPasswordRequest;
import com.runrace.server.model.connection.response.api.ApproveRecoverPasswordResponse;
import com.runrace.server.services.RunraceService;

public interface ApproveRecoverPasswordService extends RunraceService<ApproveRecoverPasswordRequest, ApproveRecoverPasswordResponse> {
}
