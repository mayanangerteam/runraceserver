package com.runrace.server.services.api.impl;

import com.runrace.server.model.*;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.api.StartRaceRequest;
import com.runrace.server.model.connection.response.api.StartRaceResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.api.intr.StartRaceService;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class StartRaceServiceImpl extends AbstractService<StartRaceRequest, StartRaceResponse> implements StartRaceService {

    private static final int MIN_HORSES = 2, MAX_HORSES = 6;
    private static final int RACE_ROUTE_LENGTH = 500;
    private static final int RACE_RACE_LENGTH = 10;
    private static final int MIN_STEP_TIMING = 50, MAX_STEP_TIMING = 150;

    @Override
    protected StartRaceResponse doExecute(StartRaceRequest request, Account account, HttpServletRequest httpServletRequest) {
        try {
            Integer size = request.getRaceSize();
            if (size == null) {
                return new StartRaceResponse(StatusCode.ERROR, ErrorCode.MISSING_DATA, null);
            }
            if (size < MIN_HORSES || size > MAX_HORSES) {
                return new StartRaceResponse(StatusCode.ERROR, ErrorCode.ILLEGAL_PARAMETER, null);
            }
            long horsesCount = getHorseRepository().count();
            if (horsesCount < size) {
                return new StartRaceResponse(StatusCode.ERROR, ErrorCode.NOT_ENOUGH_HORSES, null);
            }
            // get randomSet ids
            // fin those randomSet horses
            Collection<Horse> horses = getHorseRepository().randomHorses(size);

            // lets create randomSet raceStep to every horse
            // Lest say every raceStep represent 10 step in the route, lest find number of race step
            int raceStepsNumber = RACE_ROUTE_LENGTH / RACE_RACE_LENGTH;
            List<RaceStep> raceSteps = new ArrayList<>();
            for (int i = 0; i < raceStepsNumber; i++) {
                RaceStep raceStep = new RaceStep();
                for (int u = 0; u < size; u++) {
                    raceStep.getClass().getMethod("setHorse"+(u+1), Integer.class).invoke(raceStep, randomInt(MIN_STEP_TIMING, MAX_STEP_TIMING));
                }
                raceSteps.add(raceStep);
            }

            // map between the randomHorses horses to the randomHorses routes
            RaceMap raceMap = new RaceMap();
            int i = 1;
            for (Horse horse : horses) {
                raceMap.getClass().getMethod("setHorseId"+(i++), Long.class).invoke(raceMap, horse.getId());
            }

            Race race = new Race();
            race.setHorses(horses);
            race.setDate(new Date());
            race.setRaceSteps(raceSteps);
            race.setRaceMap(raceMap);

            getRaceRepository().save(race);

            return new StartRaceResponse(StatusCode.OK, ErrorCode.NO_ERROR, race);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return new StartRaceResponse(StatusCode.ERROR, ErrorCode.UNKNOWN, null);
        }
    }

    public int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
