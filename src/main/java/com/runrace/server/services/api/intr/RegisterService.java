package com.runrace.server.services.api.intr;

import com.runrace.server.model.connection.request.api.RegisterRequest;
import com.runrace.server.model.connection.response.api.RegisterResponse;
import com.runrace.server.services.RunraceService;

public interface RegisterService extends RunraceService<RegisterRequest, RegisterResponse> {
}
