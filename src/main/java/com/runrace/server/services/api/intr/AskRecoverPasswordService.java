package com.runrace.server.services.api.intr;

import com.runrace.server.model.connection.request.api.AskRecoverPasswordRequest;
import com.runrace.server.model.connection.response.api.AskRecoverPasswordResponse;
import com.runrace.server.services.RunraceService;

public interface AskRecoverPasswordService extends RunraceService<AskRecoverPasswordRequest, AskRecoverPasswordResponse> {
}
