package com.runrace.server.services.api.impl;

import com.runrace.server.model.Account;
import com.runrace.server.model.PasswordRecover;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.api.AskRecoverPasswordRequest;
import com.runrace.server.model.connection.response.api.AskRecoverPasswordResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.api.intr.AskRecoverPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Consumer;

public class AskRecoverPasswordServiceImpl extends AbstractService<AskRecoverPasswordRequest, AskRecoverPasswordResponse> implements AskRecoverPasswordService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected AskRecoverPasswordResponse doExecute(AskRecoverPasswordRequest request, Account account, HttpServletRequest httpServletRequest) {
        Optional<Account> accountOptional = getAccountRepository().findByEmail(request.getEmail());

        if (!accountOptional.isPresent()) {
            return new AskRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.NOT_FOUND);
        }
        account = accountOptional.get();

        Optional<PasswordRecover> passwordRecoverOptional = getPasswordRecoverRepository().findByAccount(account);
        PasswordRecover passwordRecover;
        passwordRecover = passwordRecoverOptional.orElseGet(PasswordRecover::new);

        String secretCode = randomKey(6);

        passwordRecover.setDate(new Date());
        passwordRecover.setSecretCode(passwordEncoder.encode(secretCode));
        passwordRecover.setAccount(account);
        passwordRecover.setActive(true);

        // TODO its update as well ?
        getPasswordRecoverRepository().save(passwordRecover);


        // TODO can we send secret by safe way ? or this is safe way
        sendMail(
                this,
                account.getEmail(),
                "Recover password RunRace !",
                "Your secret code: " + secretCode,
                null,
                (e)->{
                    // TODO what happen when failed sent email
                    /*Message message = new Message();
                    message.setHeader("Fail send email to user/admin after added");
                    String body = "";
                    if(!isNullOrEmpty(belongValue.getName())) {
                        body += "name : "+belongValue.getName()+"/n";
                    }
                    body += "email : "+email+"\npassword : "+password;
                    message.setBody(body);
                    message.setFrom(fromRequestBelong);
                    message.setTo(fromRequestBelong);
                    bundle.getMessageRepository().save(message);*/
                } );
        return new AskRecoverPasswordResponse(StatusCode.OK, ErrorCode.NO_ERROR);
    }

    // -------------------- Tools --------------------

    public static final String SMTP_PASSWORD = "password", SMTP_HOST = "smtp.mail.com" , SMTP_FROM ="machilon@mail.com", SMTP_USERNAME = SMTP_FROM;
    public static final int SMTP_PORT = 25;

    public static void sendMail(AbstractService service, String to, String subjson, String content, Consumer<Void> done, Consumer<Exception> fail) {
        service.getExecutor().execute(() -> {
            try {
                sendMailNative(service.getMailSender(), to, subjson, content);
                if(done!=null) {
                    done.accept(null);
                }
            }catch (Exception e) {
                if(fail!=null) {
                    fail.accept(e);
                }
            }
        });
    }

    public static void sendMailNative(JavaMailSender javaMailSender, String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(SMTP_FROM);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        javaMailSender.send(message);
    }

    // ---------- Simple Tools ----------

    public static String randomKey(int size) {
        List<String> group = new ArrayList<>();
        char c = 'A';
        do {
            group.add(String.valueOf(c));
            c++;
        }while (c<='Z');
        for (int i=0; i<10; i++) {
            group.add(String.valueOf(i));
        }
        return random(group, size);
    }

    public static String random(List<String> group, int size) {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            builder.append(group.get(random.nextInt(group.size())));
        }
        return builder.toString();
    }
}
