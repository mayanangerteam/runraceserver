package com.runrace.server.services.api.impl;

import com.runrace.server.model.Account;
import com.runrace.server.model.PasswordRecover;
import com.runrace.server.model.Patterns;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.api.ApproveRecoverPasswordRequest;
import com.runrace.server.model.connection.response.api.ApproveRecoverPasswordResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.api.intr.ApproveRecoverPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.regex.Pattern;

public class ApproveRecoverPasswordServiceImpl extends AbstractService<ApproveRecoverPasswordRequest, ApproveRecoverPasswordResponse> implements ApproveRecoverPasswordService {

    private static final int MINUTES_FOR_EXPIRED = 20;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected ApproveRecoverPasswordResponse doExecute(ApproveRecoverPasswordRequest request, Account account, HttpServletRequest httpServletRequest) {
        String newPassword = request.getNewPassword();
        String secretCode = request.getSecretCode();
        String email = request.getEmail();

        if (newPassword == null || secretCode == null || email == null) {
            return new ApproveRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.MISSING_DATA);
        }

        if (!Pattern.matches(Patterns.PASSWORD_PATTERN, newPassword)) {
            return new ApproveRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.PASSWORD_SYNTAX);
        }

        account = getAccountRepository().findByEmail(email).orElse(null);
        if (account == null) {
            return new ApproveRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.NOT_FOUND);
        }

        PasswordRecover passwordRecover = getPasswordRecoverRepository().findByAccount(account).orElse(null);
        if (passwordRecover == null || passwordRecover.getSecretCode() == null) {
            return new ApproveRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.UNEXPECTED_RECOVER);
        }

        long diff = passwordRecover.getDate().getTime() - new Date().getTime();
        long diffMinutes = diff / (60 * 1000) % 60;
        // passwordRecover is active, he need to be active And being in under expire time
        if(diffMinutes > MINUTES_FOR_EXPIRED || !passwordRecover.getActive()) {
            return new ApproveRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.CODE_EXPIRED);
        }

        if(!passwordEncoder.matches(secretCode, passwordRecover.getSecretCode())) {
            return new ApproveRecoverPasswordResponse(StatusCode.ERROR, ErrorCode.WRONG_CODE);
        }

        passwordRecover.setActive(false);
        // For secure we will make in unactive
        getPasswordRecoverRepository().save(passwordRecover);

        // Done auth, lets update password
        account.setPassword(passwordEncoder.encode(newPassword));
        getAccountRepository().save(account);

        return new ApproveRecoverPasswordResponse(StatusCode.OK, ErrorCode.NO_ERROR);
    }
}
