package com.runrace.server.services.api.intr;

import com.runrace.server.model.connection.request.api.StartRaceRequest;
import com.runrace.server.model.connection.response.api.StartRaceResponse;
import com.runrace.server.services.RunraceService;

public interface StartRaceService extends RunraceService<StartRaceRequest, StartRaceResponse> {
}
