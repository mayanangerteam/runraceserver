package com.runrace.server.services.api.impl;

import com.runrace.server.model.Account;
import com.runrace.server.model.Patterns;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.api.RegisterRequest;
import com.runrace.server.model.connection.response.api.RegisterResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.api.intr.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

public class RegisterServiceImpl extends AbstractService<RegisterRequest, RegisterResponse> implements RegisterService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected RegisterResponse doExecute(RegisterRequest request, Account account, HttpServletRequest httpServletRequest) {

        if (request.getAccount() == null || request.getAccount().getPassword() == null || request.getAccount().getEmail() == null) {
            return new RegisterResponse(StatusCode.ERROR, ErrorCode.MISSING_DATA, null);
        }
        if (!Pattern.matches(Patterns.EMAIL_PATTERN, request.getAccount().getEmail())) {
            return new RegisterResponse(StatusCode.ERROR, ErrorCode.EMAIL_SYNTAX, null);
        }
        if (!Pattern.matches(Patterns.PASSWORD_PATTERN, request.getAccount().getPassword())) {
            return new RegisterResponse(StatusCode.ERROR, ErrorCode.PASSWORD_SYNTAX, null);
        }
        Account u = new Account();
        u.setEmail(request.getAccount().getEmail());
        u.setPassword(passwordEncoder.encode(request.getAccount().getPassword()));

        try {
            u = getAccountRepository().save(u);
        } catch (Exception e) {
            if(isDuplicateException(e)) {
                // duplicate email
                return new RegisterResponse(StatusCode.ERROR, ErrorCode.DUPLICATE, null);
            }
            // unknown exception
            return new RegisterResponse(StatusCode.ERROR, ErrorCode.UNKNOWN, null);
        }
        return new RegisterResponse(StatusCode.OK, ErrorCode.NO_ERROR, u);
    }

}
