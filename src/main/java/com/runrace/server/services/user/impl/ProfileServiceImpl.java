package com.runrace.server.services.user.impl;

import com.runrace.server.model.Account;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.user.ProfileRequest;
import com.runrace.server.model.connection.response.user.ProfileResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.user.intr.ProfileService;

import javax.servlet.http.HttpServletRequest;

public class ProfileServiceImpl extends AbstractService<ProfileRequest, ProfileResponse> implements ProfileService {
    @Override
    protected ProfileResponse doExecute(ProfileRequest request, Account account, HttpServletRequest httpServletRequest) {
        return new ProfileResponse(StatusCode.OK, ErrorCode.NO_ERROR, account);
    }
}
