package com.runrace.server.services.user.intr;

import com.runrace.server.model.connection.request.user.GetHorsesRequest;
import com.runrace.server.model.connection.response.user.GetHorsesResponse;
import com.runrace.server.services.RunraceService;

public interface GetHorsesService extends RunraceService<GetHorsesRequest, GetHorsesResponse> {
}
