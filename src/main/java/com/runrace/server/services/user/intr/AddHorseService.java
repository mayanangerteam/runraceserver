package com.runrace.server.services.user.intr;

import com.runrace.server.model.connection.request.user.AddHorseRequest;
import com.runrace.server.model.connection.response.user.AddHorseResponse;
import com.runrace.server.services.RunraceService;

public interface AddHorseService extends RunraceService<AddHorseRequest, AddHorseResponse> {
}
