package com.runrace.server.services.user.impl;

import com.runrace.server.model.Account;
import com.runrace.server.model.Horse;
import com.runrace.server.model.Patterns;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.user.AddHorseRequest;
import com.runrace.server.model.connection.response.user.AddHorseResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.user.intr.AddHorseService;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

public class AddHorseServiceImpl extends AbstractService<AddHorseRequest, AddHorseResponse> implements AddHorseService {
    @Override
    protected AddHorseResponse doExecute(AddHorseRequest request, Account account, HttpServletRequest httpServletRequest) {

        Horse horse = request.getHorse();
        if (horse == null || horse.getPrivateName() == null || horse.getNickname() == null) {
            return new AddHorseResponse(StatusCode.ERROR, ErrorCode.MISSING_DATA, null);
        }

        // name and private name will be in syntax
        if (!(
                Pattern.matches(Patterns.REGULAR_PATTERN, horse.getNickname()) &&
                Pattern.matches(Patterns.REGULAR_PATTERN, horse.getPrivateName())
        )) {
            return new AddHorseResponse(StatusCode.ERROR, ErrorCode.NAME_SYNTAX, null);
        }
        // Horse race is not require for having a horse, but its must be in syntax
        if (horse.getRace() != null && !Pattern.matches(Patterns.REGULAR_PATTERN, horse.getRace())) {
            if (horse.getRace().isEmpty()) {
                horse.setRace(null);
            } else {
                return new AddHorseResponse(StatusCode.ERROR, ErrorCode.NAME_SYNTAX, null);
            }
        }

        horse.setAccount(account);

        horse = getHorseRepository().save(horse);
        return new AddHorseResponse(StatusCode.OK, ErrorCode.NO_ERROR, horse);
    }
}
