package com.runrace.server.services.user.impl;

import com.runrace.server.model.Account;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.user.GetHorsesRequest;
import com.runrace.server.model.connection.response.user.GetHorsesResponse;
import com.runrace.server.services.AbstractService;
import com.runrace.server.services.user.intr.GetHorsesService;

import javax.servlet.http.HttpServletRequest;

public class GetHorsesServiceImpl extends AbstractService<GetHorsesRequest, GetHorsesResponse> implements GetHorsesService {
    @Override
    protected GetHorsesResponse doExecute(GetHorsesRequest request, Account account, HttpServletRequest httpServletRequest) {
        return new GetHorsesResponse(StatusCode.OK, ErrorCode.NO_ERROR, getHorseRepository().findByAccount(account));
    }
}
