package com.runrace.server.services.user.intr;

import com.runrace.server.model.connection.request.user.ProfileRequest;
import com.runrace.server.model.connection.response.user.ProfileResponse;
import com.runrace.server.services.RunraceService;

public interface ProfileService extends RunraceService<ProfileRequest, ProfileResponse> {
}
