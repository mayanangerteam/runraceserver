package com.runrace.server.services;

import com.runrace.server.model.connection.request.Request;
import com.runrace.server.model.connection.response.Response;

import javax.servlet.http.HttpServletRequest;

/**
 * Abstract solution for rest api service in this app, impl by abstract service {@link AbstractService}
 * @param <Req>
 * @param <Res>
 */
public interface RunraceService<Req extends Request, Res extends Response> {
    /**
     *
     * @param request
     * @return the response in the rest api
     */
    Res execute(Req request, HttpServletRequest httpServletRequest);

}
