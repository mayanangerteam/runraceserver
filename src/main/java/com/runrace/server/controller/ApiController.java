package com.runrace.server.controller;

import com.runrace.server.model.Urls;
import com.runrace.server.model.connection.request.api.ApproveRecoverPasswordRequest;
import com.runrace.server.model.connection.request.api.AskRecoverPasswordRequest;
import com.runrace.server.model.connection.request.api.RegisterRequest;
import com.runrace.server.model.connection.request.api.StartRaceRequest;
import com.runrace.server.model.connection.response.api.ApproveRecoverPasswordResponse;
import com.runrace.server.model.connection.response.api.AskRecoverPasswordResponse;
import com.runrace.server.model.connection.response.api.RegisterResponse;
import com.runrace.server.model.connection.response.api.StartRaceResponse;
import com.runrace.server.services.api.intr.ApproveRecoverPasswordService;
import com.runrace.server.services.api.intr.AskRecoverPasswordService;
import com.runrace.server.services.api.intr.RegisterService;
import com.runrace.server.services.api.intr.StartRaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(Urls.API_SEGMENT)
public class ApiController {

    @Autowired
    private ApproveRecoverPasswordService approveRecoverPasswordService;
    @Autowired
    private AskRecoverPasswordService askRecoverPasswordService;
    @Autowired
    private RegisterService registerService;
    @Autowired
    private StartRaceService startRaceService;

    @RequestMapping(method = RequestMethod.POST, value = Urls.APPROVE_RECOVER_PASSWORD_SEGMENT)
    public ApproveRecoverPasswordResponse approveRecoverPassword(@RequestBody ApproveRecoverPasswordRequest request, HttpServletRequest httpRequest) {
        return approveRecoverPasswordService.execute(request, httpRequest);
    }

    @RequestMapping(method = RequestMethod.POST, value = Urls.ASK_RECOVER_PASSWORD_SEGMENT)
    public AskRecoverPasswordResponse askRecoverPassword(@RequestBody AskRecoverPasswordRequest request, HttpServletRequest httpRequest) {
        return askRecoverPasswordService.execute(request, httpRequest);
    }

    @RequestMapping(method = RequestMethod.POST, value = Urls.REGISTER_SEGMENT)
    public RegisterResponse register(@RequestBody RegisterRequest request, HttpServletRequest httpRequest) {
        return registerService.execute(request, httpRequest);
    }

    @RequestMapping(method = RequestMethod.POST, value = Urls.START_RACE_SEGMENT)
    public StartRaceResponse startRace(@RequestBody StartRaceRequest request, HttpServletRequest httpRequest) {
        return startRaceService.execute(request, httpRequest);
    }

}
