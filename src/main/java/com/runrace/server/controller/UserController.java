package com.runrace.server.controller;

import com.runrace.server.model.Urls;
import com.runrace.server.model.connection.request.user.AddHorseRequest;
import com.runrace.server.model.connection.request.user.GetHorsesRequest;
import com.runrace.server.model.connection.request.user.ProfileRequest;
import com.runrace.server.model.connection.response.user.AddHorseResponse;
import com.runrace.server.model.connection.response.user.GetHorsesResponse;
import com.runrace.server.model.connection.response.user.ProfileResponse;
import com.runrace.server.services.user.intr.AddHorseService;
import com.runrace.server.services.user.intr.GetHorsesService;
import com.runrace.server.services.user.intr.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(Urls.API_SEGMENT + "/" + Urls.USER_SEGMENT)
public class UserController {

    @Autowired
    private AddHorseService addHorseService;
    @Autowired
    private GetHorsesService getHorsesService;
    @Autowired
    private ProfileService profileService;

    @RequestMapping(method = RequestMethod.POST, value = Urls.ADD_HORSE_SEGMENT)
    public AddHorseResponse addHorse(@RequestBody AddHorseRequest request, HttpServletRequest httpRequest) {
        return addHorseService.execute(request, httpRequest);
    }

    @RequestMapping(method = RequestMethod.POST, value = Urls.GET_HORSES_SEGMENT)
    public GetHorsesResponse getHorses(@RequestBody GetHorsesRequest request, HttpServletRequest httpRequest) {
        return getHorsesService.execute(request, httpRequest);
    }

    @RequestMapping(method = RequestMethod.POST, value = Urls.PROFILE_SEGMENT)
    public ProfileResponse profile(@RequestBody ProfileRequest request, HttpServletRequest httpRequest) {
        return profileService.execute(request, httpRequest);
    }

}
