package com.runrace.server.application.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.runrace.server.application.security.accountimpl.AccountAuditorAware;
import com.runrace.server.model.Account;
import com.runrace.server.model.Horse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JsonConfiguration {

    @Autowired
    public void setConfiguration(ObjectMapper objectMapper) {

        PropertyFilter accountFilter = new SimpleBeanPropertyFilter() {
            @Override
            public void serializeAsField(
                    Object pojo,
                    JsonGenerator jgen,
                    SerializerProvider provider,
                    PropertyWriter writer
            ) throws Exception {

                if (include(writer)) {

                    String propertyName = writer.getName();

                    switch (propertyName) {
                        case "email":
                            Account account = AccountAuditorAware.getSessionAccount();
                            if (account!=null && account.equals(pojo)) {
                                writer.serializeAsField(pojo, jgen, provider);
                            }
                            return;
                        case "password":
                            return;
                        default:
                            writer.serializeAsField(pojo, jgen, provider);
                    }
                } else if (!jgen.canOmitFields()) { // since 2.3
                    writer.serializeAsOmittedField(pojo, jgen, provider);
                }

            }

            @Override
            protected boolean include(BeanPropertyWriter writer) {
                return true;
            }
            @Override
            protected boolean include(PropertyWriter writer) {
                return true;
            }
        };

        PropertyFilter horseFilter = new SimpleBeanPropertyFilter() {
            @Override
            public void serializeAsField(
                    Object pojo,
                    JsonGenerator jgen,
                    SerializerProvider provider,
                    PropertyWriter writer
            ) throws Exception {

                if (include(writer)) {

                    String propertyName = writer.getName();

                    switch (propertyName) {
                        case "privateName":
                            Account account = AccountAuditorAware.getSessionAccount();
                            if (account!=null && account.equals(((Horse) pojo).getAccount())) {
                                writer.serializeAsField(pojo, jgen, provider);
                            }
                            return;
                        default:
                            writer.serializeAsField(pojo, jgen, provider);
                    }
                } else if (!jgen.canOmitFields()) { // since 2.3
                    writer.serializeAsOmittedField(pojo, jgen, provider);
                }

            }

            @Override
            protected boolean include(BeanPropertyWriter writer) {
                return true;
            }
            @Override
            protected boolean include(PropertyWriter writer) {
                return true;
            }
        };

        SimpleFilterProvider filters = new SimpleFilterProvider();
        filters.addFilter("accountFilter", accountFilter);
        filters.addFilter("horseFilter", horseFilter);
        objectMapper.setFilterProvider(filters);
    }

}
