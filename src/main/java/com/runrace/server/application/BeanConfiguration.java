package com.runrace.server.application;

import com.runrace.server.application.security.accountimpl.AccountAuditorAware;
import com.runrace.server.model.Account;
import com.runrace.server.services.api.impl.ApproveRecoverPasswordServiceImpl;
import com.runrace.server.services.api.impl.AskRecoverPasswordServiceImpl;
import com.runrace.server.services.api.impl.RegisterServiceImpl;
import com.runrace.server.services.api.impl.StartRaceServiceImpl;
import com.runrace.server.services.api.intr.ApproveRecoverPasswordService;
import com.runrace.server.services.api.intr.AskRecoverPasswordService;
import com.runrace.server.services.api.intr.RegisterService;
import com.runrace.server.services.api.intr.StartRaceService;
import com.runrace.server.services.user.impl.AddHorseServiceImpl;
import com.runrace.server.services.user.impl.GetHorsesServiceImpl;
import com.runrace.server.services.user.impl.ProfileServiceImpl;
import com.runrace.server.services.user.intr.AddHorseService;
import com.runrace.server.services.user.intr.GetHorsesService;
import com.runrace.server.services.user.intr.ProfileService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Created by mayan on 27/09/2017.
 */
@Configuration
public class BeanConfiguration {

    /*Services*/

    @Bean
    public AddHorseService getAddHorseService() {
        return new AddHorseServiceImpl();
    }

    @Bean
    public GetHorsesService getHorsesService() {
        return new GetHorsesServiceImpl();
    }

    @Bean
    public ProfileService getProfileService() {
        return new ProfileServiceImpl();
    }

    @Bean
    public ApproveRecoverPasswordService getApproveRecoverPasswordService() {
        return new ApproveRecoverPasswordServiceImpl();
    }

    @Bean
    public AskRecoverPasswordService getAskRecoverPasswordService() {
        return new AskRecoverPasswordServiceImpl();
    }

    @Bean
    public RegisterService getRegisterService() {
        return new RegisterServiceImpl();
    }

    @Bean
    public StartRaceService getStartRaceService() {
        return new StartRaceServiceImpl();
    }

    /* Other Beans */

    @Bean
    public AuditorAware<Account> myAuditorProvider() {
        return new AccountAuditorAware();
    }

    @Bean
    public Executor getExecutor() {
        /*Simple setting, not the best for serious product*/
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("MailSenderThreader");
        executor.initialize();
        return executor;
    }

    private static final String
            SMTP_PASSWORD = "password",
            SMTP_HOST = "smtp.mail.com",
            SMTP_FROM ="machilon@mail.com",
            SMTP_USERNAME = SMTP_FROM;
    private static final int
            SMTP_PORT = 25;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(SMTP_HOST);
        mailSender.setPort(SMTP_PORT);

        mailSender.setUsername(SMTP_USERNAME);
        mailSender.setPassword(SMTP_PASSWORD);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
