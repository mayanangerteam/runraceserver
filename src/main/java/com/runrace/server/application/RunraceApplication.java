package com.runrace.server.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.runrace.server")
public class RunraceApplication {

	public static void main(String[] args) {
        SpringApplication.run(RunraceApplication.class, args);
	}

}
