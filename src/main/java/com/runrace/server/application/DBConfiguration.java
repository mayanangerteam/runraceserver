package com.runrace.server.application;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by mayan on 27/09/2017.
 */
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.runrace.server"})
@EnableJpaRepositories(basePackages = {"com.runrace.server"})
@EnableTransactionManagement
public class DBConfiguration {

}