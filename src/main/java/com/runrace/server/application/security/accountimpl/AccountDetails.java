package com.runrace.server.application.security.accountimpl;

import com.runrace.server.model.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public final class AccountDetails implements UserDetails {

    private static final long serialVersionUID = 1L;

    private Account account;

    public AccountDetails(Account belong) {
        setAccount(belong);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        /* TODO get permissions
        if(getBelong() instanceof Admin) {
            for (PermissionRole permissionRole : ((Admin) getBelong()).getAssignment().getPermissions()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(permissionRole.name()));
            }
        }*/
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return getAccount().getPassword();
    }

    @Override
    public String getUsername() {
        return getAccount().getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
