/*
 * Copyright 2014-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.runrace.server.application.security;

import com.runrace.server.model.Urls;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class OAuth2ServerConfiguration {

    public static final String RESOURCE_ID = "restservice";

    @Configuration
    @EnableResourceServer
    @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.resourceId(RESOURCE_ID);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()

                    // Api
                    .antMatchers("/" + Urls.APPROVE_RECOVER_PASSWORD_PATH)
                    .permitAll()

                    .antMatchers("/" + Urls.ASK_RECOVER_PASSWORD_PATH)
                    .permitAll()

                    .antMatchers("/" + Urls.REGISTER_PATH)
                    .permitAll()

                    .antMatchers("/" + Urls.START_RACE_PATH)
                    .permitAll()

                    // Account - User

                    .antMatchers("/" + Urls.ADD_HORSE_PATH)
                    .authenticated()

                    .antMatchers("/" + Urls.GET_HORSES_PATH)
                    .authenticated()

                    .antMatchers("/" + Urls.PROFILE_PATH)
                    .authenticated()
            ;
        }

    }

}
