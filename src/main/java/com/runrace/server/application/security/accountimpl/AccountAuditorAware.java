package com.runrace.server.application.security.accountimpl;

import com.runrace.server.model.Account;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AccountAuditorAware implements AuditorAware<Account> {

    public static Account getSessionAccount() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated() || !(authentication.getPrincipal() instanceof AccountDetails)) {
            return null;
        }
        return ((AccountDetails) authentication.getPrincipal()).getAccount();
    }

    public Account getCurrentAuditor() {
        return getSessionAccount();
    }
}
