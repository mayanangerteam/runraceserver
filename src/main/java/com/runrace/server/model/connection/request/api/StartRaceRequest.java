package com.runrace.server.model.connection.request.api;

import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.request.Request;

public class StartRaceRequest extends Request {

    /**
     * number of horses
     */
    private Integer raceSize;

    public StartRaceRequest(Integer raceSize) {
        this.raceSize = raceSize;
    }

    public StartRaceRequest() {
        this(null);
    }

    public Integer getRaceSize() {
        return raceSize;
    }

    public void setRaceSize(Integer raceSize) {
        this.raceSize = raceSize;
    }

}
