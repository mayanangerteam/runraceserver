package com.runrace.server.model.connection.request.api;

import com.runrace.server.model.Account;
import com.runrace.server.model.connection.request.Request;

public class RegisterRequest extends Request {

    private Account account;

    public RegisterRequest(Account account) {
        this.account = account;
    }

    public RegisterRequest() {
        this(null);
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
