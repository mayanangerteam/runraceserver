package com.runrace.server.model.connection.request.user;

import com.runrace.server.model.Horse;
import com.runrace.server.model.connection.request.Request;

public class AddHorseRequest extends Request {

    private Horse horse;

    public AddHorseRequest(Horse horse) {
        this.horse = horse;
    }

    public AddHorseRequest() {
        this(null);
    }

    public Horse getHorse() {
        return horse;
    }

    public void setHorse(Horse horse) {
        this.horse = horse;
    }
}
