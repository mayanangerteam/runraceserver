package com.runrace.server.model.connection.request.api;

import com.runrace.server.model.connection.request.Request;

public class ApproveRecoverPasswordRequest extends Request {

    private String email;
    private String newPassword;
    private String secretCode;

    public ApproveRecoverPasswordRequest(String email, String newPassword, String secretCode) {
        this.email = email;
        this.newPassword = newPassword;
        this.secretCode = secretCode;
    }

    public ApproveRecoverPasswordRequest() {
        this(null, null, null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }
}
