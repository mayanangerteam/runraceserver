package com.runrace.server.model.connection.response.user;

import com.runrace.server.model.Horse;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.response.Response;

import java.util.Collection;

public class GetHorsesResponse extends Response {

    private Collection<Horse> horses;

    public GetHorsesResponse(StatusCode statusCode, ErrorCode errorCode, Collection<Horse> horses) {
        super(statusCode, errorCode);
        this.horses = horses;
    }

    public GetHorsesResponse() {
        this(null, null, null);
    }

    public Collection<Horse> getHorses() {
        return horses;
    }

    public void setHorses(Collection<Horse> horses) {
        this.horses = horses;
    }
}
