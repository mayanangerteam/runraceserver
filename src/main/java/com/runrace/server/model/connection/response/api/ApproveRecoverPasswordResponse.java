package com.runrace.server.model.connection.response.api;

import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.response.Response;

public class ApproveRecoverPasswordResponse extends Response {

    public ApproveRecoverPasswordResponse(StatusCode statusCode, ErrorCode errorCode) {
        super(statusCode, errorCode);
    }

    public ApproveRecoverPasswordResponse() {
        this(null, null);
    }

}
