package com.runrace.server.model.connection.response;

import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;

public class Response {

    private StatusCode statusCode;
    private ErrorCode errorCode;

    public Response(StatusCode statusCode, ErrorCode errorCode) {
        this.statusCode = statusCode;
        this.errorCode = errorCode;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
