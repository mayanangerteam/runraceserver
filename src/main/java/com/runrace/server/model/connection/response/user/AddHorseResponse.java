package com.runrace.server.model.connection.response.user;

import com.runrace.server.model.Horse;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.response.Response;

public class AddHorseResponse extends Response {

    private Horse horse;

    public AddHorseResponse(StatusCode statusCode, ErrorCode errorCode, Horse horse) {
        super(statusCode, errorCode);
        this.horse = horse;
    }

    public AddHorseResponse() {
        this(null, null, null);
    }

    public Horse getHorse() {
        return horse;
    }

    public void setHorse(Horse horse) {
        this.horse = horse;
    }
}
