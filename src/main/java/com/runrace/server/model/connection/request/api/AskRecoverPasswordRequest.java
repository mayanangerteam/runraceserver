package com.runrace.server.model.connection.request.api;

import com.runrace.server.model.connection.request.Request;

public class AskRecoverPasswordRequest extends Request {

    private String email;

    public AskRecoverPasswordRequest(String email) {
        this.email = email;
    }

    public AskRecoverPasswordRequest() {
        this(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
