package com.runrace.server.model.connection.response.user;

import com.runrace.server.model.Account;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.response.Response;

public class ProfileResponse extends Response {

    private Account account;

    public ProfileResponse(StatusCode statusCode, ErrorCode errorCode, Account account) {
        super(statusCode, errorCode);
        this.account = account;
    }

    public ProfileResponse() {
        this(null, null, null);
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
