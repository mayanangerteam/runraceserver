package com.runrace.server.model.connection.response.api;

import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.response.Response;

public class AskRecoverPasswordResponse extends Response {

    public AskRecoverPasswordResponse(StatusCode statusCode, ErrorCode errorCode) {
        super(statusCode, errorCode);
    }

    public AskRecoverPasswordResponse() {
        this(null, null);
    }

}
