package com.runrace.server.model.connection.response.api;

import com.runrace.server.model.Race;
import com.runrace.server.model.connection.ErrorCode;
import com.runrace.server.model.connection.StatusCode;
import com.runrace.server.model.connection.response.Response;

public class StartRaceResponse extends Response {

    private Race race;

    public StartRaceResponse(StatusCode statusCode, ErrorCode errorCode, Race race) {
        super(statusCode, errorCode);
        this.race = race;
    }

    public StartRaceResponse() {
        this(null, null, null);
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }
}
