package com.runrace.server.model.connection;

public enum ErrorCode {
    MISSING_DATA,
    NO_ERROR,
    EMAIL_SYNTAX,
    PASSWORD_SYNTAX,
    UNKNOWN,
    DUPLICATE,
    NAME_SYNTAX,
    NOT_FOUND,
    UNEXPECTED_RECOVER,
    WRONG_CODE,
    CODE_EXPIRED,
    ILLEGAL_PARAMETER,
    NOT_ENOUGH_HORSES,
}
