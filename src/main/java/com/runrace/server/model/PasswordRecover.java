package com.runrace.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class PasswordRecover extends AbstractModel {

    private Account account;
    private String secretCode;
    private Date date;
    private Boolean active;

    public PasswordRecover(Long id, Long version, Account account, String secretCode, Date date, Boolean active) {
        super(id, version);
        this.account = account;
        this.secretCode = secretCode;
        this.date = date;
        this.active = active;
    }

    public PasswordRecover() {
        this(null, null, null, null, null, null);
    }

    @OneToOne
    @JoinColumn(nullable = false, unique = true)
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Column(nullable = false)
    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }

    @Column(nullable = false)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(nullable = false)
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
