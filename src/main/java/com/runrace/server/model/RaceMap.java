package com.runrace.server.model;

import javax.persistence.Entity;

@Entity
public class RaceMap extends AbstractModel {

    private Long horseId1;
    private Long horseId2;
    private Long horseId3;
    private Long horseId4;
    private Long horseId5;
    private Long horseId6;

    public RaceMap(Long id, Long version, Long horseId1, Long horseId2, Long horseId3, Long horseId4, Long horseId5, Long horseId6) {
        super(id, version);
        this.horseId1 = horseId1;
        this.horseId2 = horseId2;
        this.horseId3 = horseId3;
        this.horseId4 = horseId4;
        this.horseId5 = horseId5;
        this.horseId6 = horseId6;
    }

    public RaceMap() {
        this(null, null, null, null, null, null, null, null);
    }

    public Long getHorseId1() {
        return horseId1;
    }

    public void setHorseId1(Long horseId1) {
        this.horseId1 = horseId1;
    }

    public Long getHorseId2() {
        return horseId2;
    }

    public void setHorseId2(Long horseId2) {
        this.horseId2 = horseId2;
    }

    public Long getHorseId3() {
        return horseId3;
    }

    public void setHorseId3(Long horseId3) {
        this.horseId3 = horseId3;
    }

    public Long getHorseId4() {
        return horseId4;
    }

    public void setHorseId4(Long horseId4) {
        this.horseId4 = horseId4;
    }

    public Long getHorseId5() {
        return horseId5;
    }

    public void setHorseId5(Long horseId5) {
        this.horseId5 = horseId5;
    }

    public Long getHorseId6() {
        return horseId6;
    }

    public void setHorseId6(Long horseId6) {
        this.horseId6 = horseId6;
    }
}
