package com.runrace.server.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@JsonFilter("accountFilter")
public class Account extends AbstractModel {

    private String email;
    private String name;
    private String password;

    public Account(Long id, Long version, String email, String name, String password) {
        super(id, version);
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public Account() {
        this(null, null, null, null, null);
    }

    @Column(nullable = false, unique = true)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(nullable = false) /*TODO add regex ? */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
