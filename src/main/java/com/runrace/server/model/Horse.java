package com.runrace.server.model;

import com.fasterxml.jackson.annotation.JsonFilter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@JsonFilter("horseFilter")
public class Horse extends AbstractModel {

    private String nickname;
    private String race;
    private Account account;
    private String privateName;

    public Horse(Long id, Long version, String name, String nickname, String race, Account account, String privateName) {
        super(id, version);
        this.nickname = nickname;
        this.race = race;
        this.account = account;
        this.privateName = privateName;
    }

    public Horse() {
        this(null, null, null, null, null, null, null);
    }

    @Column
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Column
    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    @ManyToOne
    @JoinColumn(nullable = false)
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Column
    public String getPrivateName() {
        return privateName;
    }

    public void setPrivateName(String privateName) {
        this.privateName = privateName;
    }
}
