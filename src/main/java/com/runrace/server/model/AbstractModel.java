package com.runrace.server.model;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractModel implements Serializable, Comparable<AbstractModel>{

    private Long id;
    private Long version;

    protected AbstractModel(Long id, Long version) {
        this.id = id;
        this.version = version;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Version
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Transient
    public boolean isHaveId() {
        return getId()!=null && getId()!=-1L;
    }

    /**
     *
     * @param obj
     * @return true if not obj is null and no id is null and they both equals id's.
     * meaning if one of those models have id == null, its return false
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof AbstractModel && equals(((AbstractModel) obj));
    }

    private boolean equals(AbstractModel model) {
        return ( getId()!=null && model!=null && getId().equals(model.getId()));
    }

    @Override
    public int compareTo(AbstractModel o) {
        if(o.getId()==null || getId()==null) {
            return -1;
        }

        if(o.getId() > getId()) {
            return -1;
        }else if(o.getId() < getId()) {
            return 1;
        }
        return 0;
    }

}
