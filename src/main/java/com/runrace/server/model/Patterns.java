package com.runrace.server.model;

public class Patterns {

    public static final int PASSWORD_MAX = 20;
    public static final int PASSWORD_MIN = 6;

    public static final String REGULAR_PATTERN = "^[^@!\"']+$";
    public static final String PASSWORD_PATTERN = "^[^@!.\"']{"+PASSWORD_MIN+","+PASSWORD_MAX+"}$";
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
    public static final String PHONE_NUMBER_PATTERN = "^(?:00|\\+?)[0-9]{6,20}$";

}
