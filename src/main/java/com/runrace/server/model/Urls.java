package com.runrace.server.model;

public class Urls {

    public static final String API_SEGMENT = "api";

    public static final String
            REGISTER_SEGMENT = "register",
            ASK_RECOVER_PASSWORD_SEGMENT = "askrecover",
            APPROVE_RECOVER_PASSWORD_SEGMENT = "approverecover",
            START_RACE_SEGMENT = "startrace";

    public static final String USER_SEGMENT = "user";

    public static final String
            PROFILE_SEGMENT = "profile",
            ADD_HORSE_SEGMENT = "addhorse",
            GET_HORSES_SEGMENT = "gethorses";

    public static final String
            REGISTER_PATH =                 API_SEGMENT + "/" + REGISTER_SEGMENT,
            ASK_RECOVER_PASSWORD_PATH =     API_SEGMENT + "/" + ASK_RECOVER_PASSWORD_SEGMENT,
            APPROVE_RECOVER_PASSWORD_PATH = API_SEGMENT + "/" + APPROVE_RECOVER_PASSWORD_SEGMENT,
            START_RACE_PATH =               API_SEGMENT + "/" + START_RACE_SEGMENT,

            PROFILE_PATH =                  API_SEGMENT + "/" + USER_SEGMENT + "/" + PROFILE_SEGMENT,
            ADD_HORSE_PATH =                API_SEGMENT + "/" + USER_SEGMENT + "/" + ADD_HORSE_SEGMENT,
            GET_HORSES_PATH =               API_SEGMENT + "/" + USER_SEGMENT + "/" + GET_HORSES_SEGMENT;

}
