package com.runrace.server.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Race extends AbstractModel {

    private Collection<Horse> horses;
    private Collection<RaceStep> raceSteps;
    private RaceMap raceMap;

    private Date date;

    public Race(Long id, Long version, Collection<Horse> horses, Collection<RaceStep> raceSteps, RaceMap raceMap, Date date) {
        super(id, version);
        this.horses = horses;
        this.raceSteps = raceSteps;
        this.raceMap = raceMap;
        this.date = date;
    }

    public Race() {
        this(null, null, null, null, null, null);
    }

    @ManyToMany
    public Collection<Horse> getHorses() {
        return horses;
    }

    public void setHorses(Collection<Horse> horses) {
        this.horses = horses;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Collection<RaceStep> getRaceSteps() {
        return raceSteps;
    }

    public void setRaceSteps(Collection<RaceStep> raceSteps) {
        this.raceSteps = raceSteps;
    }

    @Column
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn
    public RaceMap getRaceMap() {
        return raceMap;
    }

    public void setRaceMap(RaceMap raceMap) {
        this.raceMap = raceMap;
    }
}
