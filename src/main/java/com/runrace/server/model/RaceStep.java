package com.runrace.server.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class RaceStep extends AbstractModel {

    /*TODO description*/
    private Integer horse1;
    private Integer horse2;
    private Integer horse3;
    private Integer horse4;
    private Integer horse5;
    private Integer horse6;

    public RaceStep(Long id, Long version, Integer horse1, Integer horse2, Integer horse3, Integer horse4, Integer horse5, Integer horse6) {
        super(id, version);
        this.horse1 = horse1;
        this.horse2 = horse2;
        this.horse3 = horse3;
        this.horse4 = horse4;
        this.horse5 = horse5;
        this.horse6 = horse6;
    }

    public RaceStep() {
        this(null,null,null,null,null,null,null,null);
    }

    @Column
    public Integer getHorse1() {
        return horse1;
    }

    public void setHorse1(Integer horse1) {
        this.horse1 = horse1;
    }

    @Column
    public Integer getHorse2() {
        return horse2;
    }

    public void setHorse2(Integer horse2) {
        this.horse2 = horse2;
    }

    @Column
    public Integer getHorse3() {
        return horse3;
    }

    public void setHorse3(Integer horse3) {
        this.horse3 = horse3;
    }

    @Column
    public Integer getHorse4() {
        return horse4;
    }

    public void setHorse4(Integer horse4) {
        this.horse4 = horse4;
    }

    @Column
    public Integer getHorse5() {
        return horse5;
    }

    public void setHorse5(Integer horse5) {
        this.horse5 = horse5;
    }

    @Column
    public Integer getHorse6() {
        return horse6;
    }

    public void setHorse6(Integer horse6) {
        this.horse6 = horse6;
    }
}
